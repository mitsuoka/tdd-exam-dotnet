﻿using Microsoft.AspNetCore.Mvc;
using SampleApp1.Models;

namespace SampleApp1.Controllers
{
    public sealed class CustomerController : Controller
    {
        // GET
        public IActionResult Index()
        {
            var customers = Customer.LoadAll();

            return View(customers);
        }
    }
}